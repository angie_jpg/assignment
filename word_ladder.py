import re
import os

def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

def find(word, words, seen, target, path, prevMatch): #HERE
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  list = sorted([(same(w, target), w) for w in list])[::-1]
  (match, item) = list[0]
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    path.append(item)
    if match >= prevMatch and find(item, words, seen, target, path, match): #HERE
      return True
    path.pop()

fname = input("Enter dictionary name: ") #gets file input from user
if len(fname) == 0: #checks if filename has no strings
  print("Error: Filename not given")
  exit(0)
elif not os.path.exists(fname): #checks if file name is non-existent
  print("Error: File does not exist")
  exit(0)
else:
  file = open(fname, "r") #opens and reads file

lines = file.readlines()
if len(lines) == 0: #checks if there is a blank file
    print("Error: File is empty")
    exit(0)

while True:
  start = input("Enter start word:")
  words = []
  if start == "": #checks if user has input a word
    print("Error: No input given")
    continue
  break

for line in lines:
  word = line.rstrip()
  if len(word) == len(start):
     words.append(word)

while True:
  target = input("Enter target word:") #user inputs target word
  if len(target) != len(start): #checks if the target word is a different length of the start word
    print("Error: Target word must be the same length of start word")
    continue
  elif target == start: #checks to see if start and target word are the same
    print("Error: start word cannot be the target word")
  break

count = 0
path = [start]
seen = {start: True}
if find(start, words, seen, target, path, 0): #HERE
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")
